---
layout: default
title: "About Chris"
---

Chris is a Computer Science Graduate student at the University
of Rhode Island. His main interests include machine learning and data
visualization.

#Work

In August of 2013, Chris completed his third internship at
[Charles River Analytics](https://www.cra.com/). He completed a
prototype written in Java, utilizing XML, RDF, OWL, OWL-S, SOAP, ReST,
and the Hadoop library.

In August of 2012, Chris completed his second internship at [The
Search Agency](http://www.thesearchagency.com/) in East Greenwich, RI.
He worked on a team with two other interns on an internal project to
load, validate, and view textual data. He coded in Java, using the GWT
framework to build a web application.

In June of 2011, Chris completed a 3-month internship for Logitech at
their Camas site. He worked on an internal product used to test
speakers to obtain subjective test results. He coded in C# and C++,
and used the .NET Framework, DirectShow APIs, and Silverlight.

#Personal

Chris enjoys riding his dual-sport when he can, and most anything
outdoors. He also enjoys a voracious appetite for learning new things,
mostly in computer science.
